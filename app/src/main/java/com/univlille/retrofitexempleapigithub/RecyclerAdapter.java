package com.univlille.retrofitexempleapigithub;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyviewHolder> {

    private List<GithubUser> liste_followers;

    private Context context;

    public RecyclerAdapter(Context context, List<GithubUser> liste_followers) {
        this.liste_followers = liste_followers;
        this.context = context;
    }

    public void setGithubUserList(List<GithubUser> liste_followers) {
        this.liste_followers = liste_followers;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerAdapter.MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.MyviewHolder holder, int position) {
//        holder.tvMovieName.setText(movieList.get(position).getTitle().toString());
        holder.login.setText(liste_followers.get(position).getLogin());
        holder.url_depot.setText(liste_followers.get(position).getUrlDepot());
        Glide.with(context)
                .load(liste_followers.get(position).getAvatarUrl())
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.avatar);
    }

    @Override
    public int getItemCount() {
        if (liste_followers != null) {
            return liste_followers.size();
        }
        return 0;
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {

        private TextView login;
        private TextView url_depot;
        private ImageView avatar;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            login = (TextView) itemView.findViewById(R.id.login);
            url_depot = (TextView) itemView.findViewById(R.id.url_depot);
            url_depot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url = url_depot.getText().toString();
                    if (!url.startsWith("http://") && !url.startsWith("https://"))
                        url = "http://" + url;

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    browserIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(browserIntent);

                }
            });
            avatar = (ImageView) itemView.findViewById(R.id.avatar);

        }
    }
}
