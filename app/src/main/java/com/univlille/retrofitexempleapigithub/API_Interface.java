package com.univlille.retrofitexempleapigithub;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface API_Interface {
    @GET("users/{username}/following")
    Call<List<GithubUser>> getFollowing(@Path("username") String username);
}
