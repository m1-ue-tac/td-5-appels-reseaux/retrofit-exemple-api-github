package com.univlille.retrofitexempleapigithub;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    List<GithubUser> liste_followers;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // TODO : améliorer le code avec une zone de saisie qui enverra son contenu à l'API pour afficher les followers de la personne saisie
    @Override
    protected void onResume() {
        super.onResume();

        liste_followers = new ArrayList<>();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerAdapter = new RecyclerAdapter(getApplicationContext(), liste_followers);
        recyclerView.setAdapter(recyclerAdapter);

        API_Interface apiService = API_Client.getClient().create(API_Interface.class);
        // plus tard on utilisera peut-être un nom d'utilisateur en paramètre
        //        Call<List<GithubUser>> call = apiService.getFollowing(username);
        // pour le moment, on appelle toujours avec le même paramètre
        Call<List<GithubUser>> call = apiService.getFollowing("JakeWharton");

        call.enqueue(new Callback<List<GithubUser>>() {
                         @Override
                         public void onResponse(Call<List<GithubUser>> call, Response<List<GithubUser>> response) {
                             Log.d("JC", "on response");
                             displayListOfUsers(response.body());
                             recyclerAdapter.setGithubUserList(response.body());
                         }

                         @Override
                         public void onFailure(Call<List<GithubUser>> call, Throwable t) {
                             Log.d("JC", "on failure");
                         }
                     }
        );
    }

    private void displayListOfUsers(List<GithubUser> users) {
        StringBuilder stringBuilder = new StringBuilder();
        for (GithubUser user : users) {
            stringBuilder.append("-" + user.getLogin() + "\n");
        }
        Log.d("JC", "réponse = \n" + stringBuilder.toString());
    }

}